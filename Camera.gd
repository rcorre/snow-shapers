extends Camera

func _enter_tree():
	add_to_group("camera")

func _physics_process(_delta: float):
	var characters := get_tree().get_nodes_in_group("character")
	var mid_z := 0.0
	for c in characters:
		mid_z += c.transform.origin.z
	if len(characters) > 0:
		var target := mid_z / len(characters)
		transform.origin.z = lerp(transform.origin.z, target, 0.1)

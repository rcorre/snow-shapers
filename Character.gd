extends KinematicBody
class_name Character

signal activated
signal died

const GRAVITY := 9.8
const DAMPING := 0.3

onready var anim: AnimationPlayer = $AnimationPlayer

var velocity := Vector3.ZERO

func _ready():
	# don't move until woken by coming on camera
	set_physics_process(false)

func activate():
	set_physics_process(true)
	emit_signal("activated")
	anim.play("Awake")
	add_to_group("character")

func die():
	set_physics_process(false)
	anim.play("Die")
	yield(anim, "animation_finished")
	emit_signal("died")
	queue_free()

func _physics_process(delta: float):
	velocity.y -= GRAVITY * delta
	velocity *= (1 - DAMPING * delta)
	velocity = move_and_slide(velocity)

	for i in get_slide_count():
		var node := get_slide_collision(i).collider as Node
		if node.is_in_group("obstacle"):
			die()

func _on_screen_exited():
	if is_physics_processing():
		die()

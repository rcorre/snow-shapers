extends Label

func _process(_delta: float):
	var cameras := get_tree().get_nodes_in_group("camera")
	if cameras:
		set_text("Distance: %d" % (cameras[0].transform.origin.z + Terrain.DEPTH / 2))

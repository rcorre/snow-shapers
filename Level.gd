extends Spatial
class_name Level

signal score_changed(new_score)

const TERRAIN_COUNT := 1
const TERRAIN_SCENE := preload("res://Terrain.tscn")

# spawn a character every this many tiles
const CHARACTER_PERIOD := 4

onready var anim: AnimationPlayer = $AnimationPlayer

var score := 0
var player_count := 1

func _on_score_changed(amount: int):
	score += amount
	emit_signal("score_changed", score)

func _ready():
	var prev: Spatial
	for i in range(TERRAIN_COUNT):
		var terrain := TERRAIN_SCENE.instance() as Spatial
		add_child(terrain)
		terrain.connect("score_changed", self, "_on_score_changed")
		terrain.connect("character_activated", self, "_on_player_activated")
		terrain.connect("character_died", self, "_on_player_died")
		terrain.global_transform.origin.z = i * Terrain.DEPTH + Terrain.DEPTH / 2
		terrain.global_transform.origin.y = -(i+1) * sin(terrain.rotation.x) * Terrain.DEPTH
		if prev:
			# neigboring terrain slices can notify eachother of the mouse position
			# since they won't catch mouse events outside their bounds
			prev.connect("mouse_moved", terrain, "on_mouse_moved")
		prev = terrain

func _on_player_activated():
	player_count += 1
	print(player_count)

func _on_player_died():
	player_count -= 1
	print(player_count)
	if player_count <= 0:
		anim.play("GameOver")
		add_child(preload("res://menus/FinishMenu.tscn").instance())

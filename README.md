# Project title goes here

Entry for [Weekly Game Jam 204](https://itch.io/jam/weekly-game-jam-204).

# Credits

- [Sniglet Font](https://www.theleagueofmoveabletype.com/sniglet) by Haley Fiege from the League of Moveable Type
- Sounds from [FreeSound](https://freesound.org)
    - [Woosh my moogy73](https://freesound.org/people/moogy73/sounds/425698/)
    - [Wind by nsstudios](https://freesound.org/people/nsstudios/sounds/479192/)
    - [Rumble by Tobiasz 'unfa' Karoń](https://freesound.org/people/unfa/sounds/258339/)
- Sounds generated with [jsfxr](http://sfxr.me)
    - ok.wav (menu sound)
    - coin.wav (menu sound)
    - hurt.wav
- Music by Eric Matyas (www.soundimage.org)
    - [Frantic Gameplay](http://soundimage.org/wp-content/uploads/2015/03/Frantic-Gameplay.mp3)

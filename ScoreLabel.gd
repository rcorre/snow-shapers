extends Label

onready var anim: AnimationPlayer = $AnimationPlayer

func set_score(value: int):
	text = "Score: %d" % value
	anim.play("Changed")

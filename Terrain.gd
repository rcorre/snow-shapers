extends MeshInstance
class_name Terrain

signal mouse_moved(global_pos)
signal score_changed(amount)
signal character_activated()
signal character_died()

const CHARACTER_SCENE := preload("res://objects/Character.tscn")

# terrain
const DEPTH := 500
const WIDTH := 40
const HEIGHT_MAX := 5.0

const OBSTACLE_STEP := 5
const CHARACTER_CHANCE := 0.04
const MIN_OBSTACLE_CHANCE := 0.2
const MAX_OBSTACLE_CHANCE := 1.0
const COIN_CHANCE := 0.1

# terrain tool
const RADIUS := 4
const FORCE := 3.0

onready var body: StaticBody = $StaticBody
onready var collider: CollisionShape = $StaticBody/CollisionShape
onready var raise_sound: AudioStreamPlayer = $RaiseSound
onready var lower_sound: AudioStreamPlayer = $LowerSound

var shape := BoxShape.new()
var height_image := Image.new()
var height_texture := ImageTexture.new()
var height_shape := HeightMapShape.new()

# map x/y index to a coin/obstacle
# used to raise/lower objects at that point on the terrain
var objects := {}

var mouse_x := 0  # x coord of the quad the mouse is over
var mouse_z := 0  # z coord of the quad the mouse is over
var mouse_in := false  # true if the mouse is on this terrain slice

func _ready():
	var plane := PlaneMesh.new()
	plane.size = Vector2(WIDTH, DEPTH)
	plane.subdivide_depth = DEPTH
	plane.subdivide_width = WIDTH
	mesh = plane
	height_image.create(WIDTH, DEPTH, false, Image.FORMAT_L8)
	height_image.fill(Color.black)
	height_texture.create_from_image(height_image)
	var shader := material_override as ShaderMaterial
	shader.set_shader_param("heightmap", height_texture)
	shader.set_shader_param("height_max", HEIGHT_MAX)

	body.connect("input_event", self, "_on_input_event")
	body.connect("mouse_entered", self, "_on_mouse_entered")
	body.connect("mouse_exited", self, "_on_mouse_exited")

	height_shape.map_data.resize(DEPTH * WIDTH)
	collider.shape = height_shape
	height_shape.map_width = WIDTH
	height_shape.map_depth = DEPTH

	place_obstacles()

	# place first character
	var node := CHARACTER_SCENE.instance() as Character
	node.connect("activated", self, "emit_signal", ["character_activated"])
	node.connect("died", self, "emit_signal", ["character_died"])
	add_child(node)
	node.transform.origin.z = -DEPTH / 2 + 1
	node.activate()

func _on_coin_collected(_by: Node):
	emit_signal("score_changed", 1)

func _remove_object_idx(idx):
	objects.erase(idx)

func place_obstacles():
	randomize()
	# randomly place some objects
	var obstacle_scenes := [
		preload("res://obstacles/Tree.tscn"),
		preload("res://obstacles/Rock.tscn"),
	]
	for z in range(10 - DEPTH / 2, DEPTH / 2, OBSTACLE_STEP):
		var x := (randi() % WIDTH) - WIDTH / 2
		var idx := (z + DEPTH / 2) * WIDTH + x + WIDTH / 2
		if randf() < COIN_CHANCE:
			var node: Spatial = preload("res://objects/Coin.tscn").instance()
			add_child(node)
			node.transform.origin = Vector3(x, 0, z)
			objects[idx] = node
			node.connect("tree_exiting", self, "_remove_object_idx", [idx])
		elif randf() < range_lerp(z, -DEPTH / 2, DEPTH / 2, MIN_OBSTACLE_CHANCE, MAX_OBSTACLE_CHANCE):
			var scene: PackedScene = obstacle_scenes[randi() % len(obstacle_scenes)]
			var node: Spatial = scene.instance()
			add_child(node)
			node.transform.origin = Vector3(x, 0, z)
			objects[idx] = node
		elif randf() < CHARACTER_CHANCE:
			var node := CHARACTER_SCENE.instance() as Character
			node.connect("activated", self, "emit_signal", ["character_activated"])
			node.connect("died", self, "emit_signal", ["character_died"])
			add_child(node)
			node.transform.origin = Vector3(x, 1, z)

func _on_input_event(_camera: Node, event: InputEvent, click_position: Vector3, _click_normal: Vector3, _shape_idx: int):
	if event is InputEventMouseMotion:
		# emit a signal so neigboring terrain slices are notified
		emit_signal("mouse_moved", click_position)
		on_mouse_moved(click_position)
		return

	var click := event as InputEventMouseButton
	if not click:
		return
	if click.button_index == BUTTON_LEFT:
		raise_sound.playing = click.pressed
	if click.button_index == BUTTON_RIGHT:
		lower_sound.playing = click.pressed

func _on_mouse_entered():
	mouse_in = true

func _on_mouse_exited():
	mouse_in = false
	raise_sound.playing = false
	lower_sound.playing = false

func on_mouse_moved(global_pos: Vector3):
	var pos := to_local(global_pos)
	mouse_x = int(round(pos.x + WIDTH / 2))
	mouse_z = int(round(pos.z + DEPTH / 2))
	mouse_in = true

func _physics_process(delta: float):
	if not mouse_in:
		return

	var terraform_y := 0
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		terraform_y = 1
	elif Input.is_mouse_button_pressed(BUTTON_RIGHT):
		terraform_y = -1
	else:
		return

	if mouse_z < -RADIUS or mouse_z > DEPTH + RADIUS:
		return

	if mouse_x < -RADIUS or mouse_x > WIDTH + RADIUS:
		return

	var changed := false
	height_image.lock()
	# form a circle around the mouse
	var zmin := max(mouse_z - 4, 0)
	var zmax := min(mouse_z + 4, DEPTH)
	for z in range(zmin, zmax):
		var x_tangent := int(round(sqrt(pow(RADIUS, 2) - pow(z - mouse_z, 2))))
		var xmin := max(mouse_x - x_tangent, 0)
		var xmax := min(mouse_x + x_tangent, WIDTH)
		for x in range(xmin, xmax):
			# amount falls of with distance from center of mouse click
			var amount := (1 - sqrt(pow(z - mouse_z, 2) + pow(x - mouse_x, 2)) / RADIUS) * terraform_y * FORCE * delta
			var color := height_image.get_pixel(x, z) + Color.white * amount
			height_image.set_pixel(x, z, color)
			height_shape.map_data[z * WIDTH + x] = height_image.get_pixel(x, z).r * HEIGHT_MAX
			changed = true
			var idx := z * WIDTH + x
			if idx in objects:
				objects[idx].transform.origin.y = height_image.get_pixel(x, z).r * HEIGHT_MAX

	height_image.unlock()
	if changed:
		height_texture.set_data(height_image)

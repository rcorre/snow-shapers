shader_type spatial;

uniform sampler2D heightmap : hint_black;
uniform float height_max = 1.0;

void vertex() {
  VERTEX.y = texture(heightmap, UV).r * height_max;
}

void fragment() {
  ALBEDO = vec3(1, 1, 1) - texture(heightmap, UV).rgb * 0.2;
}

extends ToolButton

export(String) var url

func _pressed():
	OS.shell_open(url)

extends Area

onready var anim: AnimationPlayer = $AnimationPlayer

func _ready():
	connect("body_entered", self, "_on_body_entered")

func _on_body_entered(_body: Node):
	anim.play("Collect")
	yield(anim, "animation_finished")
	queue_free()

extends Area

signal entered

func _ready():
	connect("body_entered", self, "_on_body_entered")
	get_parent().connect("activated", self, "queue_free")

func _on_body_entered(body: Node):
	if body != get_parent():
		emit_signal("entered")
